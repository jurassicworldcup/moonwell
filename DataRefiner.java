import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataRefiner {
    public static void main(String[] argv) throws Exception {
        String encoding = "UTF-8";
        BufferedReader reader = null;

        Calendar startCal = Calendar.getInstance();
        String startTime = "" + startCal.get(Calendar.HOUR_OF_DAY) +
                ":" + startCal.get(Calendar.MINUTE) +
                ":" + startCal.get(Calendar.SECOND);
        System.out.println("Start to refine wikidata... " + startTime);

        File dataFolder = new File("/home/jurassic/WikipediaPageTrafficStaticV3/");
        for (final File fileEntry : dataFolder.listFiles()) {
            if (fileEntry.isDirectory()) {
                continue;
            }

            LinkedHashMap<String, Integer> pageViewCountMap = new LinkedHashMap<String, Integer>();
            System.out.println("Now, " + fileEntry.getName());
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileEntry), encoding));
                // Picks en contry code lines.
                String reString = "^(en)(\\s)(.*)(\\s)(\\d+)(\\s)";
                Pattern enPattern = Pattern.compile(reString);
                for (String line; (line = reader.readLine()) != null; ) {
                    Matcher enMatcher = enPattern.matcher(line);
                    if (enMatcher.find()) {
                        String pageName = enMatcher.group(3);
                        int pageCount = Integer.parseInt(enMatcher.group(5));
                        if (!pageViewCountMap.containsKey(pageName)) {
                            pageViewCountMap.put(pageName, pageCount);
                        } else {
                            int currentCount = pageViewCountMap.get(pageName);
                            currentCount = currentCount + pageCount;
                            pageViewCountMap.put(pageName, currentCount);
                        }
                    }

                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                reader.close();
            }

            // Look-up table file.
            File refinedLookupFile = new File(fileEntry.getName() + "-refined-lookup");
            if (!refinedLookupFile.exists()) {
                System.out.println("Create " + refinedLookupFile.getName());
                refinedLookupFile.createNewFile();
            }

            LinkedHashMap<Integer, Integer> pageIndexViewCountMap = new LinkedHashMap<Integer, Integer>();
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(refinedLookupFile), encoding));
                int pageIndex = 0;
                for (String key : pageViewCountMap.keySet()) {
                    writer.write(key);
                    // Change page name to index.
                    pageIndexViewCountMap.put(pageIndex++, pageViewCountMap.get(key));
                    writer.newLine();
                }
            } catch (Exception e) {
                System.out.println("Exception occur.");
                System.out.println(e.getMessage());
            } finally {
                System.out.println("Writing finished.");
                writer.close();
            }

            List<Integer> pageSequenceList = new ArrayList<Integer>();
            for (Integer key : pageIndexViewCountMap.keySet()) {
                for (int i = 0; i < pageIndexViewCountMap.get(key); i++) {
                    pageSequenceList.add(key);
                }
            }
            Collections.shuffle(pageSequenceList);

            // Refined random sequence.
            File refinedFile = new File(fileEntry.getName() + "-refined");
            if (!refinedFile.exists()) {
                System.out.println("Create " + refinedFile.getName());
                refinedFile.createNewFile();
            }

            BufferedWriter sequenceWriter = null;
            try {
                sequenceWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(refinedFile), encoding));
                for (int pageIndex : pageSequenceList) {
                    sequenceWriter.write("" + pageIndex);
                    sequenceWriter.newLine();
                }
            } catch (Exception e) {
                System.out.println("Exception occur.");
                System.out.println(e.getMessage());
            } finally {
                System.out.println("Writing finished.");
                sequenceWriter.close();
            }
        }

        Calendar endCal = Calendar.getInstance();
        String endTime = "" + endCal.get(Calendar.HOUR_OF_DAY) +
                ":" + endCal.get(Calendar.MINUTE) +
                ":" + endCal.get(Calendar.SECOND);
        System.out.println("Finish refining  wikidata... " + endTime);
    }
}
