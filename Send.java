import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.*;
import java.io.IOException;
import java.lang.InterruptedException;
import java.lang.Override;
import java.lang.Runnable;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Send {

  private final static String QUEUE_NAME = "jurassic";
  private final static String ENCODING = "UTF-8";
  private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
  private static ConnectionFactory mConnectionFactory;
  private static Connection mConnection;
  private static Channel mChannel;
  private static ArrayList<Thread> mThreadList;

  public static void main(String[] argv) throws Exception {
    mConnectionFactory = new ConnectionFactory();
    mConnectionFactory.setHost("localhost");
    mConnection = mConnectionFactory.newConnection();
    mChannel = mConnection.createChannel();
    mChannel.queueDeclare(QUEUE_NAME, false, false, false, null);

    mThreadList = new ArrayList<>();

    BufferedReader reader = null;

    try {
        File dataFolder = new File("20110201_00-05/");
        File[] dataFiles = dataFolder.listFiles();
        Arrays.sort(dataFiles);

        int currentThreadIndex = 0;
        for (final File fileEntry : dataFiles) {
            if (fileEntry.isDirectory())
                continue;
            System.out.println("Start loading lookup file: " + fileEntry.getName() + " " +
                    DATE_FORMAT.format(Calendar.getInstance().getTime()));
            File lookupFile = new File(dataFolder.getName() + "/lookup/" + fileEntry.getName() + "-lookup");
            HashMap<Integer, String> indexPageMap = new HashMap<Integer, String>();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(lookupFile), ENCODING));
            int pageIndex = 0;
            for (String line; (line = reader.readLine()) != null;) {
                indexPageMap.put(pageIndex++, line);
            }
            System.out.println("End loading lookup file: " + fileEntry.getName() + " " +
                    DATE_FORMAT.format(Calendar.getInstance().getTime()));

            // Run reading. Each sendingThread must keep execution order. Run after last thread is ended.
            Thread sendingThread = new Thread(new MessageSenderRunnable(indexPageMap, fileEntry, currentThreadIndex++));
            mThreadList.add(sendingThread);
            sendingThread.start();
        }

    } finally {
        reader.close();
    }

    for (Thread thread : mThreadList) {
        thread.join();
    }

    // FIXME: MessageSenderRunnable can use channel and connection currently.
    mChannel.close();
    mConnection.close();
  }

  static class MessageSenderRunnable implements Runnable {
      private HashMap<Integer, String> mIndexPageMap;
      private final File mStreamFile;
      private final int mSendingThreadIndex;

      public MessageSenderRunnable(HashMap<Integer, String> indexPageMap, final File streamFile, int sendingThreadIndex) {
          mIndexPageMap = indexPageMap;
          mStreamFile = streamFile;
          mSendingThreadIndex = sendingThreadIndex;
      }

      @Override
      public void run() {
          BufferedReader reader = null;
          try {
              // Wait the last thread ended.
              if (mSendingThreadIndex != 0) {
                  mThreadList.get(mSendingThreadIndex - 1).join();
              }

              System.out.println("Start sending data: " + mStreamFile.getName() + " " +
                      DATE_FORMAT.format(Calendar.getInstance().getTime()));
              reader = new BufferedReader(new InputStreamReader(new FileInputStream(mStreamFile), ENCODING));
              for (String line; (line = reader.readLine()) != null; ) {
                  String keyword = mIndexPageMap.get(Integer.parseInt(line));
                  String timestamp = Long.toString(System.currentTimeMillis());
                  String data = keyword + "|" + timestamp;
                  mChannel.basicPublish("", QUEUE_NAME, null, data.getBytes("UTF-8"));
              }
              reader.close();
          } catch (IOException e) {
              System.out.println("IOException: " + e.getMessage());
          } catch (InterruptedException e) {
              System.out.println("InterruptedException: " + e.getMessage());
          }
          System.out.println("Start sending data: " + mStreamFile.getName() + " " +
                  DATE_FORMAT.format(Calendar.getInstance().getTime()));
      }
  }
}

